#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <fighter.h>
#include <personview.h>

namespace Ui {
class MainWidget;
}

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);

    ~MainWidget();

private:
    Ui::MainWidget *ui;
    Fighter *fighter1;
    Fighter *fighter2;
    PersonView *personW;

    void fillAppState();
private slots:
    void simulateAttackedSlot();
    void testPriority();
};

#endif // MAINWIDGET_H
