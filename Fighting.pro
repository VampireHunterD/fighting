#-------------------------------------------------
#
# Project created by QtCreator 2018-08-19T15:56:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Fighting
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    armor.cpp \
    fighter.cpp \
    personview.cpp \
    progressbar.cpp \
    useful.cpp \
    mainwidget.cpp \
    fight.cpp \
    body/arm.cpp \
    body/body.cpp \
    body/bodypart.cpp \
    body/compositebodypart.cpp \
    body/leg.cpp \
    skill.cpp \
    situation.cpp \
    couch.cpp \
    item.cpp \
    opposition.cpp \
    mastery.cpp \
    character_generator/charactergeneratorw.cpp \
    progressw.cpp \
    appstate.cpp \
    skilladder.cpp

HEADERS += \
        mainwidget.h \
    armor.h \
    fighter.h \
    personview.h \
    progressbar.h \
    useful.h \
    fight.h \
    body/arm.h \
    body/body.h \
    body/bodypart.h \
    body/compositebodypart.h \
    body/leg.h \
    skill.h \
    situation.h \
    couch.h \
    item.h \
    opposition.h \
    mastery.h \
    character_generator/charactergeneratorw.h \
    progressw.h \
    appstate.h \
    skilladder.h

FORMS += \
        mainwidget.ui \
    personview.ui \
    character_generator/charactergeneratorw.ui \
    progressw.ui

QMAKE_CXXFLAGS += -std=c++11

SUBDIRS += \
    body \
    Fighting.pro

DISTFILES += \
    Details

INCLUDEPATH += \
    body \
    external \

RESOURCES += \
    resources/res1.qrc

QT += uitools
