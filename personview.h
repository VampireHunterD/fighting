#ifndef PERSONVIEW_H
#define PERSONVIEW_H

#include <QWidget>
#include <fighter.h>

namespace Ui {
class PersonView;
}

class PersonView : public QWidget
{
    Q_OBJECT

public:
    explicit PersonView(QWidget *parent = 0);
    ~PersonView();

    //UILabel *headHPL;
//    UILabel *headDodgeL;
    void updateContent();
    Ui::PersonView *ui;
    void fillWithFighter(Fighter *fighter);
private:
    Fighter *_fighter;
    void updateHeadAndNeck(Fighter *fighter);
    void updateArms(Fighter *fighter);
    void updateBody(Fighter *fighter);
    void updateLegs(Fighter *fighter);

    void spreadLevelsForSkills(int levelCount, int maxSpentLevelsForIteration, QList <Skill *> skills);
private slots:
    void generateHPslot();
    void generateAdaptationSlot();
    void generateOppositionSlot();
    void generateMasterySlot();
};

#endif // PERSONVIEW_H
