#include "mainwidget.h"
#include <QApplication>

#include <QDateTime>
/*
реаля логиря срми Qt: https://evileg.com/ru/post/154/
#include "easylogging++.h"

INITIALIZE_EASYLOGGINGPP*/

void testRandomity() {
    int rolledCounts[100];
    for (int i = 0; i < 100; ++i) {
        rolledCounts[i] = 0;
    }
    int b;
    for(int i = 0; i < 100000; ++i) {
        b = randomIntAtIntervalIncludingBeginEnd(0,100);
        rolledCounts[b] = rolledCounts[b] + 1;
    }
    printf("results:");
    for (int i = 0; i < 100; ++i) {
        printf("%d %d \n",i, rolledCounts[i]);
    }
}

void runTests() {
//    testRandomity();
//    testPriority();
}



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWidget w;
    //w.show();

    runTests();
    return a.exec();
}

