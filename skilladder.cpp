#include "skilladder.h"
#include "fighter.h"
#include "skill.h"
#include "appstate.h"

// http://doc.qt.io/qt-5/qglobalstatic.html

Q_GLOBAL_STATIC(SkillAdder, skillAdder)

SkillAdder *SkillAdder::instance()
{
    return skillAdder();
}

//      получаемый за ХОД опыт будет расчитываться Fight-ом
//      выбор что будет делать перс и какие абилки юзать выбирается Fight-ом
//      потерю энергии за юзание абилки тоже расчитывает Fight

Skill * SkillAdder::addEnergyRestorationSkillToOwner(Fighter *fighter) {
    Skill *s = new Skill();
    s->name = "Восстановление энергии";
    s->hint = "Восстанавливает некоторое количество энергии. Чем больше у вас энергии, тем больше действий можно совершить за один ход. Имеет разброс от 1 до максимума";
    s->cost = 0;
    s->setMaxLevel(1000);
    s->setLevel(RANGED_INT(85,105));
    s->actionF = [s](Fighter *owner) {
        AppState::instance()->a = 5;
        int restoredEnergy = RANGED_INT(1,s->level());
        owner->restoreEnergy(restoredEnergy);
    };
    fighter->skills.append(s);
    return s;
}

//{
//    Skill *s = new Skill();
//    s->name = "Атака";
//    s->hint = "Наносит обычный урон врагу. Максимальный урон равен уровню этого скила + урону оружия. Имеет разброс урона от 1 до максимума. При потере одной руки берет оружие в другую. При потере обоих рук дерется другими частями тела, но гораздо слабее";
//    s->cost = 10;
//    s->setLevel(RANGED_INT(1,5));
//    s->setMaxLevel(100);
//    s->actionF = [s](AppState *appstate) {
//        int damage = RANGED_INT(1,100+s->level());
//        appstate->fight->defender->getDamageToBodypartFromAttacker(damage,appstate->fight->defender->randomAliveBodypart(),appstate->fight->
//}
//{
//    Skill *s = new Skill();
//    s->name = "Защита";
//    s->hint = "Делает атаку врага гарантированно попадающей, но блокирует количество урона";
//    s->cost = 10;attacker);
//    };
//    s->setMaxLevel(1);
//    s->setLevel(1);
//    s->actionF = [](AppState *appstate) {
//        int damage = RANGED_INT(1,100);
//        appstate->fight->defender->getDamageToBodypartFromAttacker(damage,appstate->fight->defender->randomAliveBodypart(),appstate->fight->attacker);
//    };
//}



//отказался от такой реализи:
//{
//    Skill *s = new Skill();
//    s->name = "Восстановление энергии";
//    s->hint = "Восстанавливает некоторое количество энергии. Чем больше у вас энергии, тем больше действий можно совершить за один ход. Имеет разброс от 1 до максимума";
//    s->cost = 0;
//    s->setMaxLevel(1000);
//    s->setLevel(RANGED_INT(85,105));
//    s->actionF = [s](AppState *appstate) {//        как обрся изнутри к владельцу этого скила? да так чтобы я мог ещё на старте игры
//                                          //        нагенерировать абилок а потом их копированием давать челам? Хотя стоп. Копированием хоть и получится но не оч всеравно. Тк часть парв все равно придется обнулять вручную. Пусть тада абила даётся методом
//        int restoredEnergy = RANGED_INT(1,s->level());
//        appstate->fight->attacker->restoreEnergy(restoredEnergy);
//    };
//}
