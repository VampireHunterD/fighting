#ifndef BODY_H
#define BODY_H

#include <QObject>
#include <compositebodypart.h>
#include <bodypart.h>

class Body : public CompositeBodypart
{
public:
    Body(QString name);
    Bodypart *chest;
    Bodypart *stomach;
    Bodypart *pelvis;

    QList <Bodypart *> bodyparts();
    QList <Bodypart *> aliveBodyparts();
};

#endif // BODY_H
