#ifndef LEG_H
#define LEG_H

#include <compositebodypart.h>
#include <bodypart.h>

class Leg : public CompositeBodypart
{
public:
    Leg(QString name);
    Bodypart *thigh;
    Bodypart *shin;
    Bodypart *foot;

    QList <Bodypart *> bodyparts();
    QList <Bodypart *> aliveBodyparts();
};

#endif // LEG_H
