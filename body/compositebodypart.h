#ifndef COMPOSITEBODYPART_H
#define COMPOSITEBODYPART_H

#include <QObject>
#include <bodypart.h>

class CompositeBodypart
{
public:
    CompositeBodypart(QString name);

    QString name;
    virtual QList <Bodypart *> bodyparts() = 0;
    virtual QList <Bodypart *> aliveBodyparts() = 0;
};

#endif // COMPOSITEBODYPART_H
