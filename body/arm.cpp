#include "arm.h"

Arm::Arm(QString name):CompositeBodypart(name)
{
    this->shoulder = new Bodypart("shoulder");
    this->forearm = new Bodypart("forearm");
    this->wrist = new Bodypart("wrist");
    this->wrist->dodgeChance = 40;
    this->forearm->dodgeChance = 10;
    this->shoulder->dodgeChance = 5;
}

QList <Bodypart *> Arm::bodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    result.append(this->shoulder);
    result.append(this->forearm);
    result.append(this->wrist);
    return result;
}

QList <Bodypart *> Arm::aliveBodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    if (this->shoulder->isAlive()) {
        result.append(this->shoulder);
    }
    if (this->forearm->isAlive()) {
        result.append(this->forearm);
    }
    if (this->wrist->isAlive()) {
        result.append(this->wrist);
    }
    return result;
}
