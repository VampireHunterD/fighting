#include "bodypart.h"

Bodypart::Bodypart(QString name) : QObject()
{
    this->name = name;
    this->setMaxAndCurHP(500);
}

bool Bodypart::isAlive() {
    bool result = this->curHP() > 0;
    return result;
}

bool Bodypart::isDamagedAlive() {
    bool result = (this->isAlive() && (this->curHP() < this->maxHP()));
    return result;
}


void Bodypart::forceDamage(int damage) {
    this->setCurHP(this->curHP() - damage);
    if (this->curHP() < 0) {
        this->setCurHP(0);
    }
}

int Bodypart::actualDodgeChance() {
    int result = 0;
    if (this->isAlive()) {
        result = ceil((double)this->curHP()/(double)this->maxHP() * (double)this->dodgeChance);//       ceil тк даже када 5 хп деталь способна уворачиваться
//        result += this->dodgeChance;
    }
    return result;
}

void Bodypart::setCurHP(int hp) {
    this->_curHP = hp;
}

void Bodypart::incHP(int hp) {
    if (this->curHP() < this->maxHP()) {
        this->_curHP += hp;
        if (this->curHP() < this->maxHP()) {
            this->_curHP = this->maxHP();
        }
    }
}

void Bodypart::repairIncHP(int hp) {
    if (this->curHP() <= 0) {
        this->_curHP += hp;
        if (this->curHP() < this->maxHP()) {
            this->_curHP = this->maxHP();
        }
    }
}
void Bodypart::setMaxHP(int hp) {
    this->_maxHP = hp;
}

void Bodypart::setMaxAndCurHP(int hp) {
    this->setMaxHP(hp);
    this->setCurHP(hp);
}

int Bodypart::curHP() {
    int result = this->_curHP;
    return result;
}

int Bodypart::maxHP() {
    int result = this->_maxHP;
    return result;
}
