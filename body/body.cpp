#include "body.h"

Body::Body(QString name):CompositeBodypart(name)
{
    this->chest = new Bodypart("chest");
    this->stomach = new Bodypart("stomach");
    this->pelvis = new Bodypart("pelvis");
    this->pelvis->dodgeChance = 5;
    this->stomach->dodgeChance = 5;
    this->chest->dodgeChance = 5;
}

QList <Bodypart *> Body::bodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    result.append(this->chest);
    result.append(this->stomach);
    result.append(this->pelvis);
    return result;
}

QList <Bodypart *> Body::aliveBodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    if (this->chest->isAlive()) {
        result.append(this->chest);
    }
    if (this->stomach->isAlive()) {
        result.append(this->stomach);
    }
    if (this->pelvis->isAlive()) {
        result.append(this->pelvis);
    }
    return result;
}
