#ifndef BODYPART_H
#define BODYPART_H

#include <QObject>
#include <item.h>

class Bodypart : public QObject
{
    Q_OBJECT
public:
    explicit Bodypart(QString name);
    QString name;
    int dodgeChance;
    QList<Item *> *activeItems;

    bool isAlive();
    bool isDamagedAlive();
    void forceDamage(int damage);
    int actualDodgeChance();// 0 when it is dead
    void setMaxAndCurHP(int hp);
    void setCurHP(int hp);
    void incHP(int hp);//       увел хп не выше максимума
    void repairIncHP(int hp);//       увел хп не выше максимума если часть мертва (чинит)
    void setMaxHP(int hp);

    int curHP();
    int maxHP();
private:
    int _maxHP;
    int _curHP;
signals:

public slots:
};

#endif // BODYPART_H
