#ifndef ARM_H
#define ARM_H

#include <QObject>
#include <compositebodypart.h>
#include <bodypart.h>

class Arm : public CompositeBodypart
{
public:
    Arm(QString name);
    Bodypart *shoulder;
    Bodypart *forearm;
    Bodypart *wrist;

    QList <Bodypart *> bodyparts();
    QList <Bodypart *> aliveBodyparts();
};

#endif // ARM_H
