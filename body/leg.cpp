#include "leg.h"

Leg::Leg(QString name):CompositeBodypart(name)
{
    this->thigh = new Bodypart("thigh");
    this->shin = new Bodypart("shin");
    this->foot = new Bodypart("foot");
    this->foot->dodgeChance = 15;
    this->shin->dodgeChance = 10;
    this->thigh->dodgeChance = 5;
}

QList <Bodypart *> Leg::bodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    result.append(this->thigh);
    result.append(this->shin);
    result.append(this->foot);
    return result;
}

QList <Bodypart *> Leg::aliveBodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    if (this->thigh->isAlive()) {
        result.append(this->thigh);
    }
    if (this->shin->isAlive()) {
        result.append(this->shin);
    }
    if (this->foot->isAlive()) {
        result.append(this->foot);
    }
    return result;
}
