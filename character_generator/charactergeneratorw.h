#ifndef CHARACTERGENERATORW_H
#define CHARACTERGENERATORW_H

#include <QWidget>

namespace Ui {
class CharacterGeneratorW;
}

class CharacterGeneratorW : public QWidget
{
    Q_OBJECT

public:
    explicit CharacterGeneratorW(QWidget *parent = 0);
    ~CharacterGeneratorW();

private:
    Ui::CharacterGeneratorW *ui;
};

#endif // CHARACTERGENERATORW_H
