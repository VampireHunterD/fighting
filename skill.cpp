#include "skill.h"

Skill::Skill() : QObject()
{
    this->name = "Not filled";
    this->hint = "Not filled";
    this->cost = 0;
    this->setLevel(0);
    this->setMaxLevel(100);
    this->chanceF = [this]() {
        int result = this->level();//       is equal by ability's level by default
        return result;
    };
    this->actionF = [](Fighter *owner){};
    this->isDetectedByCouch = NO;
}

bool Skill::isLearned() {
    bool result = (this->chanceF() > 0);
    return result;
}

void Skill::setLevel(int level) {
    this->_level = level;
    if (this->_level < MIN_LEVEL) {
        this->_level = MIN_LEVEL;
    }
    if (this->_level > this->maxLevel()) {
        this->_level = this->maxLevel();
    }
}

void Skill::setMaxLevel(int level) {
    this->_maxLevel = level;
    if (this->_maxLevel < MIN_LEVEL) {
        this->_maxLevel = MIN_LEVEL;
    }
    if (this->level() > this->_maxLevel) {
        this->setLevel(this->_maxLevel);
    }
}

int Skill::level() {
    int result = _level;
    return result;
}

int Skill::maxLevel() {
    int result = this->_maxLevel;
    return result;
}

//int Skill::chance() {
//    int result = this->level();//       по дефолту пусть верть срабя равна уровню абилки
//    return result;
//}

//int Skill::value() {
//    int result = this->_level;
//    return result;
//}

/*Skill * Skill::copy() {//     даже если буду сюда тип передавать котй нужно создать - я алгоритм котй сам класс
 * скила в себе инкапсулирует - не м скопировать тк он лежит кодом в классе. а я х чтобы они не цеплялись к этим
 * классам допно а чтобы каждый класс скила знал все что нужно чтобы совершить его робу. Просто ему на вход дают
 * то с кем нужно это сделать и он робит и всё
    Skill *result = new Skill();
    result->name = this->name;
    result->chance = this->chance;
    result->cost = this->cost;
    result->setLevel(this->level());
    result->isDetectedByCouch = this->isDetectedByCouch;
    return result;
}*/
