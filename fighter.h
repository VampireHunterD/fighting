#ifndef FIGHTER_H
#define FIGHTER_H

#include <QObject>
#include "bodypart.h"
#include "arm.h"
#include "body.h"
#include "leg.h"
#include "skill.h"
#include "item.h"
#include "situation.h"
#include "opposition.h"
#include "mastery.h"

class Fighter : public QObject
{
    Q_OBJECT
public:
    QList <Situation *> adaptation;
    QList <Opposition *> opposition;
    QList <Mastery *> mastery;
    QList <Skill *> skills;//      навыки котм чел уже овладел
    //      нейтральные
    int defaultTurnSpeed;

    explicit Fighter();

    QString name;

    int curEnergy;
    int maxEnergy;
    void restoreEnergy(int count);
    int strikeEnergyCost;//     стоимость сил на атаку у каждого бойца своя тк кто то жирнее а кто то нет и кто то больнее а кто то нет
    Bodypart *head;
    Bodypart *neck;
    Arm *arm1;
    Arm *arm2;
    Body *body;
    Leg *leg1;
    Leg *leg2;

    //      чтобы не маяться с перечислениями у чела будут сразу все возме абилы просто верть части из
    //      них будет 0 и эт знач что абилы у чела нету. Хотя моно и явный флаг поставить
    //      если навык не делает на врага явного дебафа или помогает себе но не хилит то это защитный
    //      навык
    //      сперва хилящие

    //      эффекты

    bool isLayingDown;
    bool counterAttackInProgress;
    //void setSkillsDisabledRemainedTurns(int turns);//   длится твой ход и врага потому так
    //int skillsDisabledRemainedTurns();
    //bool canUseSkills();//      м ли чел юзать все скиллы (НЕСБИВАЕМЫЕ он м юзать всегда)

    int curTurnValue;

    QList <Skill *> defaultSkills;
    QList <Skill *> appearableSkills;
    QList <Skill *> stableSkills;

//    void attack(Fighter *target);
//    void getAttack(Fighter *attacker);

    int curTurnSpeed();
    void decCurEnergy(int howMuch);
    void startRandomMutation();
    bool haveEnergy(int howMany);


    Bodypart * findAliveTargetBodypartForAttack();
    QList <Bodypart *> bodyparts();
    QList <Bodypart *> aliveBodyparts();
    QList <Bodypart *> damagedAliveBodyparts();
    Bodypart * randomBodypart();
    Bodypart * randomAliveBodypart();
    Bodypart * randomDamagedAliveBodypart();
    int dodgeChanceOfBodypart(Bodypart *bodypart);
    int standUpChance();

    QList <Item *> items();//     одетые на героя

    bool enoughEnergyOnSkills(QList <Skill *> skills);

    void getDamageToBodypartFromAttacker(int damage, Bodypart *bodypart, Fighter *attacker);

signals:

public slots:

protected:
    QList <Situation *> generateRandomAdaptation();
private:
    int _skillsDisabledRemainedTurns;//      как много ходов еще вырублены абилы

//    bool tryDodgeFromAttack(Fighter *attacker, Bodypart *targetBodypart);
    int bodyPlusLegsActualDodgeChance();
    int legsActualDodgeChance();
};

#endif // FIGHTER_H
