#include "personview.h"
#include "ui_personview.h"
#include <progressbar.h>
#include <QUiLoader>
#include <QFile>
#include <QDir>
#include <progressw.h>
#include "ui_progressw.h"
#include "appstate.h"

PersonView::PersonView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PersonView)
{
    ui->setupUi(this);

    //std::cout << s1->name << s2->name;
//    printf("names: %s %s",s1->name, s2->name);

    connect(ui->generateHPB,SIGNAL(clicked(bool)), this, SLOT(generateHPslot()));
    connect(ui->generateAdaptationB,SIGNAL(clicked(bool)), this, SLOT(generateAdaptationSlot()));
    connect(ui->generateOppositionB,SIGNAL(clicked(bool)), this, SLOT(generateOppositionSlot()));
    connect(ui->generateMasteryB,SIGNAL(clicked(bool)), this, SLOT(generateMasterySlot()));
}

void PersonView::generateHPslot() {
    _fighter->head->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(200,450));
    _fighter->neck->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(150,400));

    _fighter->arm1->shoulder->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(350,650));
    _fighter->arm1->forearm->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(300,550));
    _fighter->arm1->wrist->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(200,450));

    _fighter->arm2->shoulder->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(350,650));
    _fighter->arm2->forearm->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(300,550));
    _fighter->arm2->wrist->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(200,450));

    _fighter->body->chest->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(1000,1750));
    _fighter->body->stomach->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(850,1450));
    _fighter->body->pelvis->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(700,1250));

    _fighter->leg1->thigh->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(550,750));
    _fighter->leg1->shin->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(500,700));
    _fighter->leg1->foot->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(300,500));

    _fighter->leg2->thigh->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(550,750));
    _fighter->leg2->shin->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(500,700));
    _fighter->leg2->foot->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(300,500));

    updateHeadAndNeck(_fighter);
    updateArms(_fighter);
    updateBody(_fighter);
    updateLegs(_fighter);
}

void PersonView::generateAdaptationSlot() {
//    QList<Situation *> adaptation;
//    BlindingSunSituation *s1 = new BlindingSunSituation();
//    BumpsEarthSituation *s2 = new BumpsEarthSituation();
//    CrowdSituation *s3 = new CrowdSituation();

//    adaptation << s1 << s2 << s3;// << m4 << m5 << m6 << m7;
//    QList <Skill *> skills;
//    skills << s1 << s2 << s3;
//    int pointsCount = skills.count()*25;
//    int maxSpentLevelsForIteration = 30;
//    this->spreadLevelsForSkills(pointsCount, maxSpentLevelsForIteration, skills);
//    _fighter->adaptation = adaptation;

//    printf("adaptationcount %i",adaptation.count());
//    printf("adaptationVL %i",ui->adaptationVL->count());
//    for (int i = 0; i < adaptation.count(); ++i) {
//        QLayoutItem *item = ui->adaptationVL->itemAt(i);
//        ProgressW *w = static_cast<ProgressW *>(item->widget());
//        w->skill = static_cast<Skill *>(adaptation[i]);
//        w->updateContent();
//    }
}

void PersonView::generateOppositionSlot() {
//    QList<Opposition *> opposition;
//    AxeOpposition *o1 = new AxeOpposition();
//    BowOpposition *o2 = new BowOpposition();
//    HammerOpposition *o3 = new HammerOpposition();
//    opposition << o1 << o2 << o3;// << m4 << m5 << m6 << m7;
//    QList <Skill *> skills;
//    skills << o1 << o2 << o3;// << m4 << m5 << m6 << m7;
//    int pointsCount = skills.count()*20;
//    int maxSpentLevelsForIteration = 30;
//    this->spreadLevelsForSkills(pointsCount, maxSpentLevelsForIteration, skills);
//    _fighter->opposition = opposition;

//    printf("oppositioncount %i",opposition.count());
//    printf("oppositionVL %i",ui->oppositionVL->count());
//    for (int i = 0; i < opposition.count(); ++i) {
//        QLayoutItem *item = ui->oppositionVL->itemAt(i);
//        ProgressW *w = static_cast<ProgressW *>(item->widget());
//        w->skill = static_cast<Skill *>(opposition[i]);
//        w->updateContent();
//    }
}

void PersonView::generateMasterySlot() {
//    QList<Mastery *> mastery;
//    AxeMastery *m1 = new AxeMastery();
//    BowMastery *m2 = new BowMastery();
//    HammerMastery *m3 = new HammerMastery();
//    ScytheMastery *m4 = new ScytheMastery();
//    SickleMastery *m5 = new SickleMastery();
//    SpearMastery *m6 = new SpearMastery();
//    SwordMastery *m7 = new SwordMastery();
//    mastery << m1 << m2 << m3 << m4 << m5 << m6 << m7;
//    QList <Skill *> skills;
//    skills << m1 << m2 << m3 << m4 << m5 << m6 << m7;
//    int pointsCount = skills.count()*20;
//    int maxSpentLevelsForIteration = 30;
//    this->spreadLevelsForSkills(pointsCount, maxSpentLevelsForIteration, skills);
//    _fighter->mastery = mastery;

//    printf("masterycount %i",mastery.count());
//    printf("masterysVL %i",ui->masteryVL->count());
//    for (int i = 0; i < mastery.count(); ++i) {
//        QLayoutItem *item = ui->masteryVL->itemAt(i);
//        ProgressW *w = static_cast<ProgressW *>(item->widget());
//        w->skill = static_cast<Skill *>(mastery[i]);
//        w->updateContent();
//    }
}

void PersonView::spreadLevelsForSkills(int levelCount, int maxSpentLevelsForIteration, QList <Skill *> skills) {
    //      начинать тоже будем с рандомного навыка а то первый скил будет чаще получать поинты и потому
    //      чаще первые скилы будут иметь поинты а осте нет
    //      если maxSpentLevelsForIteration будет слишком большим то вероятно на 1й же навык часто уйдут все поинты
    int remainedLevels = levelCount;
    int curIndex;
    int levelsForSpending;
    Skill *curSkill;
    bool curSkillReachedMaxLevel;
    //
    while (remainedLevels > 0 && skills.count() > 0) {
        curIndex = randomIntAtIntervalIncludingBeginEnd(0,skills.count() - 1);
        curSkill = skills[curIndex];
        levelsForSpending = randomIntAtIntervalIncludingBeginEnd(0,maxSpentLevelsForIteration);
        if (levelsForSpending > remainedLevels) {// низя потратить больше чем осталось )
            levelsForSpending = remainedLevels;
        }
        curSkillReachedMaxLevel = NO;
        if (curSkill->level() + levelsForSpending > curSkill->maxLevel()) {
            levelsForSpending = curSkill->maxLevel() - curSkill->level();
            curSkillReachedMaxLevel = YES;
        }
        curSkill->setLevel(curSkill->level() + levelsForSpending);
        remainedLevels -= levelsForSpending;
        if (curSkillReachedMaxLevel) {//        удаляем скилл из рассмя тк его больше не апнуть
            skills.removeAt(curIndex);
        }
    }
    //      есть верть что заполнишь скилы до предела а оставшиеся поинты улетят в трубу (хотя нафиг они нужны)
}

PersonView::~PersonView()
{
    delete ui;
}

void PersonView::updateHeadAndNeck(Fighter *fighter) {
    ui->headHPB->setMaxValue(fighter->head->maxHP());
    ui->headHPB->setCurValue(fighter->head->curHP());
    ui->headDodgeL->setText(QString::number(fighter->head->actualDodgeChance()));

    ui->neckHPB->setMaxValue(fighter->neck->maxHP());
    ui->neckHPB->setCurValue(fighter->neck->curHP());
    ui->neckDodgeL->setText(QString::number(fighter->neck->actualDodgeChance()));
}

void PersonView::updateArms(Fighter *fighter) {
    ui->shoulder1HPB->setMaxValue(fighter->arm1->shoulder->maxHP());
    ui->shoulder1HPB->setCurValue(fighter->arm1->shoulder->curHP());
    ui->shoulder1DodgeL->setText(QString::number(fighter->arm1->shoulder->actualDodgeChance()));

    ui->shoulder2HPB->setMaxValue(fighter->arm2->shoulder->maxHP());
    ui->shoulder2HPB->setCurValue(fighter->arm2->shoulder->curHP());
    ui->shoulder2DodgeL->setText(QString::number(fighter->arm2->shoulder->actualDodgeChance()));

    ui->forearm1HPB->setMaxValue(fighter->arm1->forearm->maxHP());
    ui->forearm1HPB->setCurValue(fighter->arm1->forearm->curHP());
    ui->forearm1DodgeL->setText(QString::number(fighter->arm1->forearm->actualDodgeChance()));

    ui->forearm2HPB->setMaxValue(fighter->arm2->forearm->maxHP());
    ui->forearm2HPB->setCurValue(fighter->arm2->forearm->curHP());
    ui->forearm2DodgeL->setText(QString::number(fighter->arm2->forearm->actualDodgeChance()));

    ui->wrist1HPB->setMaxValue(fighter->arm1->wrist->maxHP());
    ui->wrist1HPB->setCurValue(fighter->arm1->wrist->curHP());
    ui->wrist1DodgeL->setText(QString::number(fighter->arm1->wrist->actualDodgeChance()));

    ui->wrist2HPB->setMaxValue(fighter->arm2->wrist->maxHP());
    ui->wrist2HPB->setCurValue(fighter->arm2->wrist->curHP());
    ui->wrist2DodgeL->setText(QString::number(fighter->arm2->wrist->actualDodgeChance()));
}

void PersonView::updateBody(Fighter *fighter) {
    ui->chestHPB->setMaxValue(fighter->body->chest->maxHP());
    ui->chestHPB->setCurValue(fighter->body->chest->curHP());
    ui->chestDodgeL->setText(QString::number(fighter->body->chest->actualDodgeChance()));

    ui->stomachHPB->setMaxValue(fighter->body->stomach->maxHP());
    ui->stomachHPB->setCurValue(fighter->body->stomach->curHP());
    ui->stomachDodgeL->setText(QString::number(fighter->body->stomach->actualDodgeChance()));

    ui->pelvisHPB->setMaxValue(fighter->body->pelvis->maxHP());
    ui->pelvisHPB->setCurValue(fighter->body->pelvis->curHP());
    ui->pelvisDodgeL->setText(QString::number(fighter->body->pelvis->actualDodgeChance()));
}

void PersonView::updateLegs(Fighter *fighter) {
    ui->thigh1HPB->setMaxValue(fighter->leg1->thigh->maxHP());
    ui->thigh1HPB->setCurValue(fighter->leg1->thigh->curHP());
    ui->thigh1DodgeL->setText(QString::number(fighter->leg1->thigh->actualDodgeChance()));

    ui->thigh2HPB->setMaxValue(fighter->leg2->thigh->maxHP());
    ui->thigh2HPB->setCurValue(fighter->leg2->thigh->curHP());
    ui->thigh2DodgeL->setText(QString::number(fighter->leg2->thigh->actualDodgeChance()));

    ui->shin1HPB->setMaxValue(fighter->leg1->shin->maxHP());
    ui->shin1HPB->setCurValue(fighter->leg1->shin->curHP());
    ui->shin1DodgeL->setText(QString::number(fighter->leg1->shin->actualDodgeChance()));

    ui->shin2HPB->setMaxValue(fighter->leg2->shin->maxHP());
    ui->shin2HPB->setCurValue(fighter->leg2->shin->curHP());
    ui->shin2DodgeL->setText(QString::number(fighter->leg2->shin->actualDodgeChance()));

    ui->foot1HPB->setMaxValue(fighter->leg1->foot->maxHP());
    ui->foot1HPB->setCurValue(fighter->leg1->foot->curHP());
    ui->foot1DodgeL->setText(QString::number(fighter->leg1->foot->actualDodgeChance()));

    ui->foot2HPB->setMaxValue(fighter->leg2->foot->maxHP());
    ui->foot2HPB->setCurValue(fighter->leg2->foot->curHP());
    ui->foot2DodgeL->setText(QString::number(fighter->leg2->foot->actualDodgeChance()));
}

void PersonView::updateContent() {
    updateHeadAndNeck(_fighter);
    updateArms(_fighter);
    updateBody(_fighter);
    updateLegs(_fighter);

    for (int i = 0; i < 20; ++i) {
        ProgressW *w1 = new ProgressW();
        ui->adaptationVL->addWidget(w1);
    }
    for (int i = 0; i < 20; ++i) {
        ProgressW *w1 = new ProgressW();
        ui->oppositionVL->addWidget(w1);
    }
    for (int i = 0; i < 20; ++i) {
        ProgressW *w1 = new ProgressW();
        ui->masteryVL->addWidget(w1);
    }
}

void PersonView::fillWithFighter(Fighter *fighter) {
    this->_fighter = fighter;
    this->updateContent();
}
