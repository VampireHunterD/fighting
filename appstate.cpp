#include "appstate.h"

// http://doc.qt.io/qt-5/qglobalstatic.html

Q_GLOBAL_STATIC(AppState, appState)

AppState *AppState::instance()
{
    return appState();
}

