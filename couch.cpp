#include "couch.h"
//#include "opposition.h"

Couch::Couch()
{

}

void Couch::researchFighterDeeper(Fighter *fighter) {
    //      наш ли это боец или не наш - пытаемся лучше его узнать
    //      детектим скилы:
    foreach (Skill *s, fighter->skills) {
        s->isDetectedByCouch = s->isDetectedByCouch && chancePassedFromChanceOnSuccess(this->researching);
    }
    //      детектим вещи:
    foreach (Item *i, fighter->items()) {
        i->isDetectedByCouch = i->isDetectedByCouch && chancePassedFromChanceOnSuccess(this->researching);
    }
    //      детектим привычные условия боя:
    foreach (Situation *s, fighter->adaptation) {
        s->isDetectedByCouch = s->isDetectedByCouch && chancePassedFromChanceOnSuccess(this->researching);
    }
    //      детектим привычные оружия против котх им проще драться

    //      детектим оппозиции к оружию
    foreach (Opposition *o, fighter->opposition) {
        o->isDetectedByCouch = o->isDetectedByCouch && chancePassedFromChanceOnSuccess(this->researching);
    }
    //      детектим навыки боя оружием
    foreach (Mastery *m, fighter->mastery) {
        m->isDetectedByCouch = m->isDetectedByCouch && chancePassedFromChanceOnSuccess(this->researching);
    }
}
