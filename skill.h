#ifndef SKILL_H
#define SKILL_H

#define MIN_LEVEL 1

#include <QObject>
#include "useful.h"
#include "skill.h"
#include "fight.h"

class AppState;
//#include "appstate.h"
//      должен ли скил знать о том как он должен коммуницировать с др скилами? по идее нет. Проще пусть робит по
//      цепочке обязанностей: ему на вход даётся игровая ситя а он делает действие, нр смотрит у сопа конкре скилы коте могут заблочить этот и
//      активируется и соотвно ставит себя в кд и скил сопа в кд
//      каждый скил в конструкторе уже будет создаваться с нужными всеми парми

//      все скилы мб по разному устроены и содт разные доп данные (каждый - свои) потому лучше робить с каждым
//      скилом как с отдельным объектом. Потому копировать по тупому не прокатит. Копирку написать тоже не получится
//      Потому лучше вручную копировать каждый раз. Думаю в общем то такие действия будут редко нужны:
//      при генерации перса, при загрузке с диска. При применении. Всё.
class Skill : public QObject
{
    Q_OBJECT
public:
    explicit Skill();
    QString name;
    QString hint;
    int cost;//     скоко энергии потратится при срабатывании
    bool isLearned();//     изучен
    void setLevel(int level);//     min level is 1!
    void setMaxLevel(int level);
    int level();
    int maxLevel();

    std::function<int()> chanceF;// 0-100

//    std::function<int()> calcValueF;// итоговая польза скила. По дефолту расчитывается по левелу.

    std::function<void(Fighter *owner)> actionF;

    bool isDetectedByCouch;//       если тренер в своем ученике или враге увидел этот скилл, то мы знаем что соп обладает им и будем знать об этом его скил все

    //      debuffs - скил тоже м иметь дебафы от сопа. или наоборот бафы. они просто вешаются и проверяются
    //      в конце хода на снятие.
    //Skill * copy();
signals:

public slots:
private:
    int _level;
    int _maxLevel;
};

#endif // SKILL_H
