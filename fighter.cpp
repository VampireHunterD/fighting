#include "fighter.h"
#include <useful.h>
#include <arm.h>
#include <body.h>
#include <leg.h>

Fighter::Fighter() : QObject()
{
    this->adaptation = this->generateRandomAdaptation();
    this->head = new Bodypart("head");
    this->neck = new Bodypart("neck");
    this->head->dodgeChance = 5;
    this->neck->dodgeChance = 5;
    this->arm1 = new Arm("arm1");
    this->arm2 = new Arm("arm2");
    this->body = new Body("body");
    this->leg1 = new Leg("leg1");
    this->leg2 = new Leg("leg2");

    this->curEnergy = 0;
    this->maxEnergy = 1000;
}

QList <Situation *> Fighter::generateRandomAdaptation() {
    QList <Situation *> result;
//    result <<  s1;
//    result <<  s2;
//    result <<  s3;
//    result <<  s4;
//    result <<  s5;
//    result <<  s6;
//    result <<  s7;
//    result <<  s8;
//    result <<  s9;
//    result <<  s10;
//    result <<  s11;
//    result <<  s12;
//    result <<  s13;
//    result <<  s14;
//    result <<  s15;
//    result <<  s16;
//    result <<  s17;
//    result << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 << s13 << s14 << s15 << s16 << s17;
//    for (int i = 0; i < result.count(); ++i) {
//        Situation *s = result[i];
//        s->setLevel(randomIntAtIntervalIncludingBeginEnd(0,100));
//    }
    return result;
}

//void Fighter::attack(Fighter *target) {
//    target->getAttack(this);
//}

//void Fighter::getAttack(Fighter *attacker) {
//    Bodypart *bodypartTargetForAttack = this->findTargetBodypartForAttack();
//    bodypartTargetForAttack->forceDamage(5);
//}

Bodypart * Fighter::findAliveTargetBodypartForAttack() {
    Bodypart *result = nullptr;
    QList <Bodypart *> aliveBodyParts = this->aliveBodyparts();
    if (aliveBodyParts.count() > 0) {
        //      знаем что с верю в 25% враг будет бить по 1 из двух самых подбитых частей.
        //      причем мультиатака в этом случае точно попадет токо по одной из 2х самы подбитых частей. по второй не факт
        bool willAttackJustOneOfTwoWeakestBodypart = chancePassedFromChanceOnSuccess(25);
        if (willAttackJustOneOfTwoWeakestBodypart) {
            //      отсортируем
            Bodypart *iBodypart;
            Bodypart *kBodypart;
            if (aliveBodyParts.count() > 1) {
                for (int i = 0; i < aliveBodyParts.count() - 1; ++i) {
                    for (int k = 1; k < aliveBodyParts.count() - 1; ++k) {
                        iBodypart = aliveBodyParts[i];
                        kBodypart = aliveBodyParts[k];
                        if (kBodypart->curHP() < iBodypart->curHP()) {
                            aliveBodyParts.swap(i,k);
                        }
                    }
                }
                bool willUseTarget1 = chancePassedFromChanceOnSuccess(50);
                if (willUseTarget1) {
                    result = aliveBodyParts[0];
                } else {
                    result = aliveBodyParts[1];
                }

            } else {
                result = aliveBodyParts[0];
            }
        } else {//      целью будет любая живая часть тела
            int randomBodypartNum = randomIntAtIntervalExcludingEndNumber(0,aliveBodyParts.count());
            randomBodypartNum = 0;
            result = aliveBodyParts[randomBodypartNum];
        }
    }
    return result;
}

QList <Bodypart *> Fighter::bodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    result.append(this->head);
    result.append(this->neck);
    result.append(this->arm1->bodyparts());
    result.append(this->arm2->bodyparts());
    result.append(this->body->bodyparts());
    result.append(this->leg1->bodyparts());
    result.append(this->leg2->bodyparts());

    return result;
}

QList <Bodypart *> Fighter::aliveBodyparts() {
    QList <Bodypart *> result = QList<Bodypart *>();
    if (this->head->isAlive()) {
        result.append(this->head);
    }
    if (this->neck->isAlive()) {
        result.append(this->neck);
    }
    result.append(this->arm1->aliveBodyparts());
    result.append(this->arm2->aliveBodyparts());
    result.append(this->body->aliveBodyparts());
    result.append(this->leg1->aliveBodyparts());
    result.append(this->leg2->aliveBodyparts());

    return result;
}

QList <Bodypart *> Fighter::damagedAliveBodyparts() {
    QList <Bodypart *> result = this->aliveBodyparts();
    int i = 0;
    Bodypart *bp;
    while (i < result.count()) {
        bp = result[i];
        if (!bp->isDamagedAlive()) {
            result.removeAt(i);
        } else {
            ++i;
        }
    }
    return result;
}

Bodypart * Fighter::randomBodypart() {
    Bodypart *result;
    QList <Bodypart *> bodyparts = this->bodyparts();
    int randomNum = randomIntAtIntervalExcludingEndNumber(0,bodyparts.count());
    result = bodyparts[randomNum];
    return result;
}

Bodypart * Fighter::randomAliveBodypart() {
    Bodypart *result;
    QList <Bodypart *> bodyparts = this->aliveBodyparts();
    int randomNum = randomIntAtIntervalExcludingEndNumber(0,bodyparts.count());
    result = bodyparts[randomNum];
    return result;
}

Bodypart * Fighter::randomDamagedAliveBodypart() {
    Bodypart *result;
    QList <Bodypart *> bodyparts = this->damagedAliveBodyparts();
    int randomNum = randomIntAtIntervalExcludingEndNumber(0,bodyparts.count());
    result = bodyparts[randomNum];
    return result;
}

//bool Fighter::tryDodgeFromAttack(Fighter *attacker, Bodypart *targetBodypart) {
//    bool result = false;
//    int summaryDodgeChange = this->dodgeChanceOfBodypart(targetBodypart);
//    result = chancePassedFromChanceOnSuccess(summaryDodgeChange);
//    return result;
//}



int Fighter::dodgeChanceOfBodypart(Bodypart *bodypart) {
    int result = 0;
    if (this->leg1->foot == bodypart) {
        result += bodypart->actualDodgeChance();
    } else if (this->leg2->foot == bodypart) {
        result += bodypart->actualDodgeChance();
    } else if (this->leg1->shin == bodypart) {
        result += this->leg1->foot->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->leg2->shin == bodypart) {
        result += this->leg2->foot->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->leg1->thigh == bodypart) {
        result += this->leg1->foot->actualDodgeChance();
        result += this->leg1->shin->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->leg2->thigh == bodypart) {
        result += this->leg2->foot->actualDodgeChance();
        result += this->leg2->shin->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->body->pelvis == bodypart) {
        result += this->legsActualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->body->stomach == bodypart) {
        result += this->legsActualDodgeChance();
        result += this->body->pelvis->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->body->chest == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
    } else if (this->neck == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->head == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += this->neck->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->arm1->shoulder == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->arm2->shoulder == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->arm1->forearm == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += this->arm1->shoulder->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->arm2->forearm == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += this->arm2->shoulder->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->arm1->wrist == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += this->arm1->shoulder->actualDodgeChance();
        result += this->arm1->forearm->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    } else if (this->arm2->wrist == bodypart) {
        result += this->bodyPlusLegsActualDodgeChance();
        result += this->arm2->shoulder->actualDodgeChance();
        result += this->arm2->forearm->actualDodgeChance();
        result += bodypart->actualDodgeChance();
    }
    return result;
}

int Fighter::standUpChance() {
    int result = 0;

    result += this->leg1->foot->actualDodgeChance();
    result += this->leg2->foot->actualDodgeChance();
    result += this->leg1->shin->actualDodgeChance();
    result += this->leg1->thigh->actualDodgeChance();
    result += this->leg2->thigh->actualDodgeChance();
    result += this->body->pelvis->actualDodgeChance();
    result += this->body->stomach->actualDodgeChance();
    result += this->body->chest->actualDodgeChance();
    result += this->arm1->shoulder->actualDodgeChance();
    result += this->arm2->shoulder->actualDodgeChance();
    result += this->arm1->forearm->actualDodgeChance();
    result += this->arm2->forearm->actualDodgeChance();
    result += this->arm1->wrist->actualDodgeChance();
    result += this->arm2->wrist->actualDodgeChance();
    result += this->neck->actualDodgeChance();
    result += this->head->actualDodgeChance();

    return result;
}

int Fighter::bodyPlusLegsActualDodgeChance() {
    int result = 0;
    result += this->legsActualDodgeChance();
    result += this->body->pelvis->actualDodgeChance();
    result += this->body->stomach->actualDodgeChance();
    result += this->body->chest->actualDodgeChance();
    return result;
}

int Fighter::legsActualDodgeChance() {
    int result = 0;
    result += this->leg1->foot->actualDodgeChance();
    result += this->leg1->shin->actualDodgeChance();
    result += this->leg1->thigh->actualDodgeChance();
    result += this->leg2->foot->actualDodgeChance();
    result += this->leg2->shin->actualDodgeChance();
    result += this->leg2->thigh->actualDodgeChance();
    return result;
}

int Fighter::curTurnSpeed() {
    int result = 0;//this->defaultTurnSpeed + this->turnSpeedSkill->value();
    return result;
}

void Fighter::restoreEnergy(int count) {
    this->curEnergy += count;
    if (this->curEnergy > this->maxEnergy) {
        this->curEnergy = this->maxEnergy;
    }
}

void Fighter::decCurEnergy(int count) {
    this->curEnergy -= count;
    if (this->curEnergy < 0) {
        this->curEnergy = 0;
    }
}

bool Fighter::haveEnergy(int howMany) {
    bool result = NO;
    if (this->curEnergy >= howMany) {
        result = YES;
    }
    return result;
}

void Fighter::startRandomMutation() {

    this->defaultTurnSpeed = randomIntAtIntervalIncludingBeginEnd(40,55);
    this->maxEnergy = randomIntAtIntervalIncludingBeginEnd(200,400);
    this->curEnergy = this->maxEnergy;

    this->head->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->neck->setMaxHP(randomIntAtIntervalIncludingBeginEnd(250,350));

    this->leg1->foot->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->leg1->shin->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->leg1->thigh->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));

    this->leg2->foot->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->leg2->shin->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->leg2->thigh->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));

    this->body->pelvis->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->body->stomach->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->body->chest->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));

    this->arm1->shoulder->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->arm1->forearm->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->arm1->wrist->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));

    this->arm2->shoulder->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->arm2->forearm->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
    this->arm2->wrist->setMaxAndCurHP(randomIntAtIntervalIncludingBeginEnd(250,450));
}
/*
void Fighter::setSkillsDisabledRemainedTurns(int turns) {
    _skillsDisabledRemainedTurns = turns;
    if (_skillsDisabledRemainedTurns < 0) {
        _skillsDisabledRemainedTurns = 0;
    }
}*/
/*
void Fighter::setHealSkillsDisabledRemainedTurns(int turns) {
    _healSkillsDisabledRemainedTurns = turns;
    if (_healSkillsDisabledRemainedTurns < 0) {
        _healSkillsDisabledRemainedTurns = 0;
    }
}

void Fighter::setDefenceSkillsDisabledRemainedTurns(int turns) {
    _defenceSkillsDisabledRemainedTurns = turns;
    if (_defenceSkillsDisabledRemainedTurns < 0) {
        _defenceSkillsDisabledRemainedTurns = 0;
    }
}

void Fighter::setAttackSkillsDisabledRemainedTurns(int turns) {
    _attackSkillsDisabledRemainedTurns = turns;
    if (_attackSkillsDisabledRemainedTurns < 0) {
        _attackSkillsDisabledRemainedTurns = 0;
    }
}*/
/*
int Fighter::skillsDisabledRemainedTurns() {
    return _skillsDisabledRemainedTurns;
}

bool Fighter::canUseSkills() {
    bool result = this->skillsDisabledRemainedTurns() == 0;
    return result;
}*/
/*
int Fighter::healSkillsDisabledRemainedTurns() {
    return _healSkillsDisabledRemainedTurns;
}

int Fighter::defenceSkillsDisabledRemainedTurns() {
    return _defenceSkillsDisabledRemainedTurns;
}

int Fighter::attackSkillsDisabledRemainedTurns() {
    return _attackSkillsDisabledRemainedTurns;
}*/

QList <Item *> Fighter::items() {
    QList <Item *> result = QList<Item *>();
    return result;
}

bool Fighter::enoughEnergyOnSkills(QList <Skill *> skills) {
    bool result = YES;
    int skillsRequireEnergy = 0;
    Skill *skill;
    for (int i = 0; i < skills.count(); ++i) {
        skill = skills[i];
        skillsRequireEnergy += skill->cost;
    }
    result = (this->curEnergy >= skillsRequireEnergy);
    return result;
}

void Fighter::getDamageToBodypartFromAttacker(int damage, Bodypart *bodypart, Fighter *attacker) {
    bodypart->forceDamage(damage);
}
