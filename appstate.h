#ifndef APPSTATE_H
#define APPSTATE_H

#include "situation.h"
#include "opposition.h"
#include "mastery.h"

#include <qglobal.h>
#include <QGlobalStatic>

class Fight;

class AppState
{
public:
    static AppState *instance();

    int a;
    QList <Skill *> defaultSkills;
    QList<Situation *> availableAdaptation;
    QList<Opposition *> availableOpposition;
    QList<Mastery *> availableMastery;

    Fight *fight;
};

#endif // APPSTATE_H
