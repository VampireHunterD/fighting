#include "mainwidget.h"
#include "ui_mainwidget.h"
#include "ui_personview.h"
#include <useful.h>
#include <QDateTime>
#include <QTimer>

#include "skilladder.h"
#include "appstate.h"

int nextRandomIndexForPriority(QList<int> priority) {
//    Skill *s1 = new Skill();
//    s1->name = "s1";
//    s1->actionF = [s1](AppState *appState) {
//        qDebug() << "a = " << appState->a;
//        qDebug() << "skill worked good";
//        appState->a = 15;
//        s1->name.append("s1");
//        qDebug() << appState->a;
//    };
//    s1->actionF(AppState::instance());
//    qDebug() << "a after = " << AppState::instance()->a;

//    Skill *s2 = new Skill();
//    s2->name = "s2";
//    s2->actionF = s1->actionF;
//    qDebug() << "a2 before = " << AppState::instance()->a;



//    AppState::instance()->a = 7;
//    qDebug() << AppState::instance()->a;
//    skill->actionF(AppState::instance());

//    auto f1 = [](int x, int y) { return x + y; };
//    int a = f1(5,10);
    int result = 0;
    QList<int> points;
    int curLeft = 0;
    points << curLeft;
    for (int i = 0; i < priority.count(); ++i) {
        curLeft += priority[i];
        points << curLeft;
    }
    int end = curLeft;
    int value = randomIntAtIntervalIncludingBeginEnd(0,end);
    int curIndex = 0;
    bool nextIndexExists;
    bool willContinue;
    while (YES) {
        nextIndexExists = (curIndex + 1 < points.count());
        willContinue = nextIndexExists && (value > points[curIndex+1]);
        if (willContinue) {
            ++curIndex;
        } else {
            break;
        }
    }
    result = curIndex;
    return result;
}

QList<int> generateRandomPriorityForCount(int count) {
    QList<int> result;
    for (int i = 0; i < count; ++i) {
        result << 1;
    }
    int b;
    int max = 10;
    int indicatorForPlus;
    for (int k = 0; k < 5; ++k) {
        printf("iteration\n");
        for(int i = 0; i < count; ++i) {
            b = randomIntAtIntervalIncludingBeginEnd(1,max);
            //printf("rand: %d\n",b);
            indicatorForPlus = randomIntAtIntervalIncludingBeginEnd(0,10);
            if (indicatorForPlus < 4) {
                result[i] = result[i] + b;
            }
        }
        max *= 2;
    }
    printf("results:\n");
    int minimalBigger0 = result[0];
    for (int i = 0; i < count; ++i) {
        printf("%d %d \n",i, result[i]);
        minimalBigger0 = result[i];
    }
//    printf("min nonzero: %d",minimalBigger0);
//    for (int i = 0; i < maxSkillCount; ++i) {
//        skills[i] = skills[i]/minimalBigger0;
//    }
//    printf("divided:\n");
//    for (int i = 0; i < maxSkillCount; ++i) {
//        printf("%d %d \n",i, skills[i]);
//    }
    return result;
}

void MainWidget::testPriority() {
    int maxSkillCount = 30;
    QList<int> priority = generateRandomPriorityForCount(maxSkillCount);
    QList<int> skills;
    for (int i = 0; i < maxSkillCount; ++i) {
        skills << 0;
    }
    for (int i = 0; i < skills.count(); ++i) {
        //printf("zero filled skills %d \n",skills[i]);
    }
    QList<int> list;
    list.append(2);
    nextRandomIndexForPriority(list);
//    int index;
//    for (int i = 0; i < 1000; ++i) {
//        index = nextRandomIndexForPriority(priority);
//        skills[index] = skills[index] + 1;
//        //printf("index %i   value here yet %i\n",index,skills[index]);

//    }
    printf("\n\nfinished skills spreading:\n");
    for (int i = 0; i < skills.count(); ++i) {
        printf("%d %d \n",i, skills[i]);
    }
}

MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    this->fighter1 = new Fighter();
    this->fighter2 = new Fighter();
    //this->fighter1->attack(this->fighter2);

    ui->setupUi(this);
    this->hide();

    this->fillAppState();

    this->personW = new PersonView;
//    Ui::PersonView ui;
//    ui.setupUi(widget);

    printf("epoch %lli",QDateTime::currentDateTime().toMSecsSinceEpoch());
    qsrand(QDateTime::currentDateTime().toMSecsSinceEpoch());//     call it just in this thread (or it willnt work)

    QTimer::singleShot(2000, this, SLOT(testPriority()));


    this->personW->show();
    this->personW->fillWithFighter(this->fighter1);

    /*qApp->setStyleSheet("QLabel {"
                        "border: 2px solid grey;"
                        "border-radius: 5px;"
                        //"height: 16px;"
                        "font: bold 10px;"
                    "}"

                        "QLabel::chunk {"
                            "background-color: #05B8CC;"
                            "width: 20px;"
                        "}");*/
    connect(this->personW->ui->attackB,SIGNAL(clicked(bool)), this, SLOT(simulateAttackedSlot()));
}

//      все же скорость хода тада тут не нужна. Ведь если энергия кончится то в этом нет смысла и плюс
//      всё сильно усложняет. Просто пусть тада сопы действуют по очереди. А вот реген энергии важен.
//      он и будет значить как часто чел действует. так что вот так.

//      предполагаю что изнно у чела 100 энергии
void MainWidget::fillAppState() {
    Skill *s = SkillAdder::instance()->addEnergyRestorationSkillToOwner(this->fighter1);
    qDebug() << "energy1" << this->fighter1->curEnergy;
    s->actionF(this->fighter1);
    qDebug() << "energy2" << this->fighter1->curEnergy;
    s->actionF(this->fighter1);
    qDebug() << "energy3" << this->fighter1->curEnergy;
    s->actionF(this->fighter1);
    qDebug() << "energy4" << this->fighter1->curEnergy;
//    BlindingSunSituation *s1 = new BlindingSunSituation();
//    BumpsEarthSituation *s2 = new BumpsEarthSituation();
//    CrowdSituation *s3 = new CrowdSituation();
//    QList<Situation *> adaptation;
//    adaptation << s1 << s2 << s3;// << m4 << m5 << m6 << m7;
//    AppState::instance()->availableAdaptation = adaptation;

//    AxeOpposition *o1 = new AxeOpposition();
//    BowOpposition *o2 = new BowOpposition();
//    HammerOpposition *o3 = new HammerOpposition();
//    QList<Opposition *> opposition;
//    opposition << o1 << o2 << o3;// << m4 << m5 << m6 << m7;
//    AppState::instance()->availableOpposition = opposition;

//    AxeMastery *m1 = new AxeMastery();
//    BowMastery *m2 = new BowMastery();
//    HammerMastery *m3 = new HammerMastery();
//    ScytheMastery *m4 = new ScytheMastery();
//    SickleMastery *m5 = new SickleMastery();
//    SpearMastery *m6 = new SpearMastery();
//    SwordMastery *m7 = new SwordMastery();
//    QList<Mastery *> mastery;
//    mastery << m1 << m2 << m3 << m4 << m5 << m6 << m7;
//    AppState::instance()->availableMastery = mastery;
}


void MainWidget::simulateAttackedSlot() {
    qDebug() << "lol";
    //this->fighter1->getAttack(this->fighter2);
    this->personW->updateContent();
}

MainWidget::~MainWidget()
{
    delete ui;
}
