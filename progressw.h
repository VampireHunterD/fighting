#ifndef PROGRESSW_H
#define PROGRESSW_H

#include <QWidget>
#include <skill.h>

namespace Ui {
class ProgressW;
}

class ProgressW : public QWidget
{
    Q_OBJECT

public:
    explicit ProgressW(QWidget *parent = 0);
    ~ProgressW();
    Skill *skill;
    void updateContent();
private:
    Ui::ProgressW *ui;
private slots:
    void incSlot();
    void decSlot();
};

#endif // PROGRESSW_H
