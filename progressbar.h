#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QObject>
#include <QWidget>
#include <QLabel>

class ProgressBar : public QLabel
{
public:
    ProgressBar(QWidget *parent = nullptr);
    void setMaxValue(int v);
    void setCurValue(int v);
    int curValue();
    int maxValue();

protected:
    void paintEvent(QPaintEvent *);
private:
    int _maxValue;
    int _curValue;
};

#endif // PROGRESSBAR_H
