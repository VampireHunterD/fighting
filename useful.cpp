#include "useful.h"

Useful::Useful()
{

}

bool chancePassedFromChanceOnSuccess(int changeOnSuccess) {
    bool result = false;
    int random = randomIntAtIntervalIncludingBeginEnd(1,100);
    if (random <= changeOnSuccess) {
        result = true;
    }
    return result;
}

int RANGED_INT(int from, int to) {
    int result = randomIntAtIntervalIncludingBeginEnd(from, to);
    return result;
}

int randomIntAtIntervalIncludingBeginEnd(int from, int to) {
    int result;
    if (to > from) {
        //qDebug() << "randomIntIncludeBeginEnd" << to << ">" << from;
    }
    int diff = to - from;
    int nonShiftedResult = qrand() % (diff + 1);
    result = nonShiftedResult + from;
    return result;
}

int randomIntAtIntervalExcludingEndNumber(int from, int to) {
    int result;
    if (to >= from) {
        //qDebug() << "randomIntIncludeBeginEnd" << to << ">=" << from;
    }
    int diff = to - from;
    int nonShiftedResult = qrand() % diff;
    result = nonShiftedResult + from;
    return result;
}
