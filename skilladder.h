#ifndef SKILLADDER_H
#define SKILLADDER_H

class Skill;
class Fighter;

class SkillAdder
{
public:
    static SkillAdder *instance();

    Skill * addEnergyRestorationSkillToOwner(Fighter *owner);
};

#endif // SKILLADDER_H
