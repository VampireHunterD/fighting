#ifndef ITEM_H
#define ITEM_H

#include <QObject>

typedef enum {
    Weapon = 0,

    HeadArmor = 1,

    ShoulderArmor = 2,
    ForearmArmor = 3,
    WristArmor = 4,

    ChestArmor = 5,
    StomachArmor = 6,
    PelvisArmor = 7,

    ThighArmor = 8,
    ShinArmor = 9,
    FootArmor = 10,
} ItemType;

class Item : public QObject
{
public:
    Item();
    ItemType kind;

    bool isDetectedByCouch;
};

#endif // ITEM_H
