#include "progressw.h"
#include "ui_progressw.h"
#include "progressbar.h"

ProgressW::ProgressW(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProgressW)
{
    ui->setupUi(this);
    setLayout(ui->rootHL);
    connect(ui->plusB,SIGNAL(clicked(bool)), this, SLOT(incSlot()));
    connect(ui->minusB,SIGNAL(clicked(bool)), this, SLOT(decSlot()));
    //this->setupLayout();
//    QVBoxLayout *layout = new QVBoxLayout;
    //        layout->addWidget(myWidget);
    //        setLayout(layout);
}

//void ProgressW::setupLayout() {
//    setLayout(ui->rootHL);
//}

void ProgressW::incSlot() {
    ui->progressBar->setCurValue(ui->progressBar->curValue() + 1);
    if (ui->progressBar->curValue() > ui->progressBar->maxValue()) {
        ui->progressBar->setCurValue(ui->progressBar->maxValue());
    }
}

void ProgressW::decSlot() {
    ui->progressBar->setCurValue(ui->progressBar->curValue() - 1);
    if (ui->progressBar->curValue() < 0) {
        ui->progressBar->setCurValue(0);
    }
}

void ProgressW::updateContent() {
    ui->nameL->setText(this->skill->name);
    ui->progressBar->setCurValue(this->skill->level());
    ui->progressBar->setMaxValue(this->skill->maxLevel());
}

ProgressW::~ProgressW()
{
    delete ui;
}
