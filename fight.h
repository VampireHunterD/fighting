#ifndef FIGHT_H
#define FIGHT_H

#include <QWidget>
#include <useful.h>

class Fighter;

class Fight : public QWidget
{
    Q_OBJECT
public:
    explicit Fight(QWidget *parent = nullptr);
    Fighter *fighter1;
    Fighter *fighter2;

    Fighter *attacker;
    Fighter *defender;

    void findTurner();
private:
    int fighter1FinishedTurnCount;//    как много ходов сделал игрок за бой
    int fighter2FinishedTurnCount;
    int fighter1TurnValue;
    int fighter2TurnValue;

    QString roundResultS;
    void startBattle();
    void findNextTurnerAndStartNextPersonTurn();
    void startNextPersonTurn(Fighter *attacker, Fighter *defender);
    void finishBattle();

signals:

public slots:
};

#endif // FIGHT_H
