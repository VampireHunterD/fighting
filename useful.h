#ifndef USEFUL_H
#define USEFUL_H

#include <QObject>
#include <QDebug>

#define YES true
#define NO false

class Useful
{
public:
    Useful();
};

bool chancePassedFromChanceOnSuccess(int changeOnSuccess);
int RANGED_INT(int from, int to);
int randomIntAtIntervalIncludingBeginEnd(int from, int to);
int randomIntAtIntervalExcludingEndNumber(int from, int to);

#endif // USEFUL_H
