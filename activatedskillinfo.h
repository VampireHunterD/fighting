#ifndef ACTIVATEDSKILLINFO_H
#define ACTIVATEDSKILLINFO_H

#include <skill.h>

class ActivatedSkillInfo
{
public:
    ActivatedSkillInfo();
    Skill *skill;
};

#endif // ACTIVATEDSKILLINFO_H
