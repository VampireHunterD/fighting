#include "progressbar.h"
#include <QPainter>

ProgressBar::ProgressBar(QWidget *parent):QLabel(parent)
{
    _maxValue = 100;
    _curValue = 30;
   /* this->setStyleSheet(/*"QLabel {"
                        "border: 2px solid grey;"
                        "border-radius: 5px;"
                        //"height: 16px;"
                        "font: bold 10px;"
                    "}"*/

                        /*"QLabel::chunk {"
                            "background-color: #05B8CC;"
                            "width: 20px;"
                        "}"*/
                       // );
    this->update();
}

void ProgressBar::setMaxValue(int v) {
    this->_maxValue = v;
    this->update();
}

void ProgressBar::setCurValue(int v) {
    this->_curValue = v;
    this->update();
}

int ProgressBar::curValue() {
    int result = this->_curValue;
    return result;
}

int ProgressBar::maxValue() {
    int result = this->_maxValue;
    return result;
}


void ProgressBar::paintEvent(QPaintEvent *)
{
    int side = qMin(width(), height());
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    int filledProgressWidth = ((float)this->_curValue / (float)this->_maxValue) * (float)width() ;

    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.setPen(QColor(0,0,0,0));
    painter.setBrush(QColor("#05B8CC"));
    QRect filledProgressRect = QRect(0,0,filledProgressWidth,height());
    //draw progress line without pen
    if (filledProgressWidth > 0) {
        painter.drawRect(filledProgressRect);
    }
    QRect borderRect = QRect(0,0,width(),height());
    int radius = 4;
    //fill useless area with transparent color
    QPainterPath path1 = QPainterPath();
    path1.addRect(filledProgressRect);
    QPainterPath path2 = QPainterPath();
    path2.addRoundedRect(borderRect,radius,radius);
    QPainterPath pathForDelete = path1 - path2;
    painter.setBrush(this->palette().base()/* Qt::NoBrush*/);
    painter.setPen(QColor(0,0,0,255));
    painter.drawPath(pathForDelete);

    //draw form without pen
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.setPen(QColor(Qt::white));
    painter.setBrush(QColor(0,0,0,0)/* Qt::NoBrush*/);
    painter.drawRoundedRect(borderRect,radius,radius);

    QRectF textRect = QRectF(0,0,width(),height());
    painter.setPen(QColor(Qt::white));
    painter.drawText(textRect, Qt::AlignCenter, QString::number(this->curValue()));

    painter.setPen(QColor(155,155,155,255));
    QRectF textMaxRect = textRect;
    textMaxRect.setWidth(textMaxRect.width() - 4);
    painter.drawText(textMaxRect, Qt::AlignRight | Qt::AlignVCenter, QString::number(this->maxValue()));
}
