#include "healskill.h"
#include "useful.h"

HealSkill::HealSkill()
{
    this->cost = 5;
}

int HealSkill::value() {
    int result = randomIntAtIntervalIncludingBeginEnd(1,100);
    return result;
}
