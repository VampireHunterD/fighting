#ifndef DISABLEENEMYATTACKSKILLSSKILL_H
#define DISABLEENEMYATTACKSKILLSSKILL_H

#include <skill.h>

//      шанс заблочить врагу атакующие скилы на след его ход
class DisableEnemyAttackSkillsSkill : public Skill
{
public:
    DisableEnemyAttackSkillsSkill();
};

#endif // DISABLEENEMYATTACKSKILLSSKILL_H
