#ifndef CHEERUPSKILL_H
#define CHEERUPSKILL_H

#include <skill.h>

//      дает халявную энергию в размере 1-100 (дополняет только до максимума своей энергии). Шанс равный вложенным очкам - что энергия будет добавлена поверх максимальной
class CheerUpSkill : public Skill
{
public:
    CheerUpSkill();
};

#endif // CHEERUPSKILL_H
