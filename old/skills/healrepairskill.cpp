#include "healrepairskill.h"
#include "useful.h"

HealRepairSkill::HealRepairSkill()
{
    this->cost = 20;
}

int HealRepairSkill::value() {
    int result = randomIntAtIntervalIncludingBeginEnd(1,50);
    return result;
}
