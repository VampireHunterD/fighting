#ifndef CRITICALSKILL_H
#define CRITICALSKILL_H

#include <skill.h>

//      крит за каждый поинт дает +1/10 урона при сраби и срабатывает чаще на 1
class CriticalSkill : public Skill
{
public:
    CriticalSkill();
};

#endif // CRITICALSKILL_H
