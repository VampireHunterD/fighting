#ifndef DISABLEENEMYDEFENCESKILLSSKILL_H
#define DISABLEENEMYDEFENCESKILLSSKILL_H

#include <skill.h>

//      шанс заблочить врагу его защитные абилки в его ход
class DisableEnemyDefenceSkillsSkill : public Skill
{
public:
    DisableEnemyDefenceSkillsSkill();
};

#endif // DISABLEENEMYDEFENCESKILLSSKILL_H
