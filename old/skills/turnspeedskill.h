#ifndef TURNSPEEDSKILL_H
#define TURNSPEEDSKILL_H

#include <skill.h>

class TurnSpeedSkill : public Skill
{
public:
    TurnSpeedSkill();
    int chance();
};

#endif // TURNSPEEDSKILL_H
