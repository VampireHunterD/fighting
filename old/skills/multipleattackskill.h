#ifndef MULTIPLEATTACKSKILL_H
#define MULTIPLEATTACKSKILL_H

#include <skill.h>

// вместо удара по 1 части тела нанести его еще и по 1-3(рандом)др частям тела
class MultipleAttackSkill : public Skill
{
public:
    MultipleAttackSkill();
};

#endif // MULTIPLEATTACKSKILL_H
