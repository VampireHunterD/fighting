#ifndef STABLEACCURACYWITHCONTUSIONSKILL_H
#define STABLEACCURACYWITHCONTUSIONSKILL_H

#include <skill.h>

//      если есть контузия (у головы <75% хп) то точность атак повышена на число вложенных очков
class AccuracyWithContusionSkill : public Skill
{
public:
    AccuracyWithContusionSkill();
};

#endif // STABLEACCURACYWITHCONTUSIONSKILL_H
