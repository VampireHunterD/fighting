#ifndef PARRYSKILL_H
#define PARRYSKILL_H

#include <skill.h>

//      парирует атаку врага. снижает прочность оружия
class ParrySkill : public Skill
{
public:
    ParrySkill();
};

#endif // PARRYSKILL_H
