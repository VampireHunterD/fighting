#ifndef HEALPERCENTSKILL_H
#define HEALPERCENTSKILL_H

#include <skill.h>

//  ЦЕЛИ: все части (и мертвые) но если попал на мертвую то не лечит. шанс починить се любую живую часть на 1-100%. Если часть цела или мертва то ниче не делает.
//  также есть верть равная вложенным поинтам, что будет починена самая подбитая часть (те что хил точно вложится выгодно)
class HealPercentSkill : public Skill
{
public:
    HealPercentSkill();
    int value();
};

#endif // HEALPERCENTSKILL_H
