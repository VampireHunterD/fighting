#ifndef CONFUSEENEMYSKILL_H
#define CONFUSEENEMYSKILL_H

#include <skill.h>

//  шанс запутать. снижает верть попадения по те на 1-100
class ConfuseEnemySkill : public Skill
{
public:
    ConfuseEnemySkill();
};

#endif // CONFUSEENEMYSKILL_H
