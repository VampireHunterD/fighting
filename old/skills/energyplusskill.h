#ifndef ENERGYPLUSSKILL_H
#define ENERGYPLUSSKILL_H

#include <skill.h>

class EnergyPlusSkill : public Skill
{
public:
    EnergyPlusSkill();
    int chance();
};

#endif // ENERGYPLUSSKILL_H
