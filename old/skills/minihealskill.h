#ifndef STABLEMINIHEALSKILL_H
#define STABLEMINIHEALSKILL_H

#include <skill.h>

//      в начале каждого твоего хода отхиливает рандомную из оставшихся частей на
//      колво вложенных поинтов. если часть цела то ниче не делает
class MiniHealSkill : public Skill
{
public:
    MiniHealSkill();
};

#endif // STABLEMINIHEALSKILL_H
