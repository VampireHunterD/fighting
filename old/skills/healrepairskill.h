#ifndef HEALREPAIRSKILL_H
#define HEALREPAIRSKILL_H

#include <skill.h>

//  все части. шанс починить часть на 1-50% только если она была разрушена
class HealRepairSkill : public Skill
{
public:
    HealRepairSkill();
    int value();
};

#endif // HEALREPAIRSKILL_H
