#ifndef STABLEATTACKPOWERSKILL_H
#define STABLEATTACKPOWERSKILL_H

#include <skill.h>

// навсегда повышает суммарную (от 2х оружий) мощь удара на колво вложенных поинтов
class AttackPowerSkill : public Skill
{
public:
    AttackPowerSkill();
};

#endif // STABLEATTACKPOWERSKILL_H
