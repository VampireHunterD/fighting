#ifndef RETURNDAMAGESKILL_H
#define RETURNDAMAGESKILL_H

#include <skill.h>

//  шанс вернуть сопу 1-100% наносимого им дмг а остальной дмг получишь ты
class ReturnDamageSkill : public Skill
{
public:
    ReturnDamageSkill();
};

#endif // RETURNDAMAGESKILL_H
