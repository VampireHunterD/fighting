#ifndef COUNTERATTACKSKILL_H
#define COUNTERATTACKSKILL_H

#include <skill.h>

//      атакуешь врага вместо его атаки. в 50% случаев враг пропускает свой ход (не ожидал)
class CounterAttackSkill : public Skill
{
public:
    CounterAttackSkill();
};

#endif // COUNTERATTACKSKILL_H
