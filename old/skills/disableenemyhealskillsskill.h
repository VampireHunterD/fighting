#ifndef DISABLEENEMYHEALSKILLSSKILL_H
#define DISABLEENEMYHEALSKILLSSKILL_H

#include <skill.h>

//      дает шанс заблочить все абилки хила на след ход врага
class DisableEnemyHealSkillsSkill : public Skill
{
public:
    DisableEnemyHealSkillsSkill();
};

#endif // DISABLEENEMYHEALSKILLSSKILL_H
