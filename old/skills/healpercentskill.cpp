#include "healpercentskill.h"
#include "useful.h"

HealPercentSkill::HealPercentSkill()
{
    this->cost = 10;
}

int HealPercentSkill::value() {
    int result = randomIntAtIntervalIncludingBeginEnd(1,100);
    return result;
}
