#ifndef HEALSKILL_H
#define HEALSKILL_H

#include <skill.h>

//      шанс подхилять раненую живую часть на 1-100хп
class HealSkill : public Skill
{
public:
    HealSkill();
    int value();
};

#endif // HEALSKILL_H
