#ifndef PLAINEARTHSITUATION_H
#define PLAINEARTHSITUATION_H

#include "situation.h"

class PlainEarthSituation : public Situation
{
public:
    PlainEarthSituation();
};

#endif // PLAINEARTHSITUATION_H
