#ifndef NIGHTSITUATION_H
#define NIGHTSITUATION_H

#include "situation.h"

class NightSituation : public Situation
{
public:
    NightSituation();
};

#endif // NIGHTSITUATION_H
