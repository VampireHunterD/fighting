#ifndef SCYTHEOPPOSITION_H
#define SCYTHEOPPOSITION_H

#include "opposition.h"

class ScytheOpposition : public Opposition
{
public:
    ScytheOpposition();
};

#endif // SCYTHEOPPOSITION_H
