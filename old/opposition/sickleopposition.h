#ifndef SICKLEOPPOSITION_H
#define SICKLEOPPOSITION_H

#include "opposition.h"

class SickleOpposition : public Opposition
{
public:
    SickleOpposition();
};

#endif // SICKLEOPPOSITION_H
