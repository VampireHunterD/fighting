#ifndef HAMMEROPPOSITION_H
#define HAMMEROPPOSITION_H

#include "opposition.h"

class HammerOpposition : public Opposition
{
public:
    HammerOpposition();
};

#endif // HAMMEROPPOSITION_H
