#ifndef SPEAROPPOSITION_H
#define SPEAROPPOSITION_H

#include "opposition.h"

class SpearOpposition : public Opposition
{
public:
    SpearOpposition();
};

#endif // SPEAROPPOSITION_H
