#include "fight.h"
#include <activatedskillinfo.h>
#include <fighter.h>

Fight::Fight(QWidget *parent) : QWidget(parent) {
    this->fighter1 = new Fighter();
    this->fighter2 = new Fighter();
    fighter1->maxEnergy = randomIntAtIntervalIncludingBeginEnd(100,310);
    fighter1->curEnergy = randomIntAtIntervalIncludingBeginEnd(0, fighter1->maxEnergy);

    fighter2->maxEnergy = randomIntAtIntervalIncludingBeginEnd(100,310);
    fighter2->curEnergy = randomIntAtIntervalIncludingBeginEnd(0, fighter2->maxEnergy);

    //      те чел просто выживает. идет и крошит врагов 1 на 1 но нападают они на него в любой
    //      момент потому он мб сразу уставший. тк каждый чел разный то он мб и больным потому
    //      макс энергии варьируется довольно сильно

    fighter1->strikeEnergyCost = randomIntAtIntervalIncludingBeginEnd(40,70);
    fighter2->strikeEnergyCost = randomIntAtIntervalIncludingBeginEnd(40,70);
    //      тк в первую очередь каждый чел копит силу на удар или на уворот, то он копит ее не на
    //      то чтобы встать потому считаем что стоимость удара всегда равная. хотя моно её
    //      варьировать в зависти от сити
    //      как видим у них разное колво энергии и затрат на удар.
    //      также у каждого своя скорость. замечу не скорость регена. а скорость хода. те тот из них
    //      кто быстрее, то будет и ходить чаще. а знач он чаще энергию восстановит на удар и
    //      сумеет опомниться в бою и победить
    //      но даже будучи быстрым ты м не иметь энергии на ещё один удар
    //      моно даже не так, а что энергия тем медленнее регенится чем ты раненнее.
    //      дамаг принципиально будет изнно зависеть токо от оружия и того наскоко живо тело.
    //      те тут не как в др играх просто взял статы распределил и все равны и от них пляшется.
    //      нихуя. у каждого своя жесткость.

//    терь нужно опрть верти всех абил и записать какие сработают и какие нет. а затем уже идти по
//            логике и писать в лог или на экран все что произошло
    this->fighter1->curTurnValue = 0;
    this->fighter2->curTurnValue = 0;
}

void Fight::startBattle() {
    this->roundResultS.clear();
    fighter1->name = "F111";
    fighter2->name = "F222";
    while (fighter1->head->isAlive() && fighter2->head->isAlive()) {
        this->findNextTurnerAndStartNextPersonTurn();
    }
    this->finishBattle();
}

void Fight::findNextTurnerAndStartNextPersonTurn() {
    if (this->fighter1->curTurnValue == this->fighter2->curTurnValue) {
        bool willTurnFighter1 = chancePassedFromChanceOnSuccess(50);
        if (willTurnFighter1) {
            this->startNextPersonTurn(fighter1, fighter2);
            this->fighter1FinishedTurnCount += 1;
        } else {
            this->startNextPersonTurn(fighter2, fighter2);
            this->fighter2FinishedTurnCount += 1;
        }
    } else if (this->fighter1->curTurnValue < this->fighter2->curTurnValue) {
        this->startNextPersonTurn(fighter1, fighter2);
        this->fighter1FinishedTurnCount += 1;
    } else {
        this->startNextPersonTurn(fighter2, fighter2);
        this->fighter2FinishedTurnCount += 1;
    }
}

void Fight::startNextPersonTurn(Fighter *attacker, Fighter *defender) {
//    QString attackerResultS;
//    QString defenderResultS;
//    //      игрок будет выбирать скиллы вручную. А соп рандомом.
//    //      срабатывать они уже будут друг за другом и тада и будут выбираться цели и тд
//    QList <Skill *> pickableAttackerActivatedSkills = QList<Skill *>();
//    QList <Skill *> pickableDefenderActivatedSkills = QList<Skill *>();
//    //  сперва обработаем все невырубаемые абилы
//    //  TURN SPEED
//    attacker->curTurnValue += defender->curTurnSpeed();//       ходит тот у кого щас зне меньше потому плюсуем в начале хода се скор врага и тада ходит бырее тот у кого оно выше
//    //  DEFAULT ENERGY ADD + ENERGY PLUS
//    if (attacker->curEnergy < attacker->maxEnergy) {
//        attacker->curEnergy += randomIntAtIntervalIncludingBeginEnd(10,100) + attacker->energyPlusSkill->value();
//        if (attacker->curEnergy > attacker->maxEnergy) {
//            attacker->curEnergy = attacker->maxEnergy;
//        }
//    }
//    //      энергию получит тот чей ход. притом если защитник делает контрудар то атакующему дадут энергию и передадут ход защитнику тк иначе атакующий мб останется совсем без энергии при частых контратаках
//    if (attacker->curEnergy > attacker->maxEnergy) {//знач она переполнена потому не трогаем её
//        attackerResultS += "Энергия максимальна, не могу пополнить.\n";
//    } else {
//        int plusEnergy = randomIntAtIntervalIncludingBeginEnd(10,100);
//        attacker->curEnergy += plusEnergy;
//        if (attacker->curEnergy > attacker->maxEnergy) {
//            attacker->curEnergy = attacker->maxEnergy;
//        }
//        attackerResultS += "Пополнил энергию на " + QString::number(plusEnergy) + ".\n";
//    }
//    //      CheerUp НЕЙТРАЛЬНЫЙ БЛОКИРУЕМЫЙ
//    if (attacker->cheerUpSkill->isLearned()) {
//        bool willAddEnergy = chancePassedFromChanceOnSuccess(attacker->cheerUpSkill->value());
//        if (willAddEnergy) {
//            int energyCountForPlus = randomIntAtIntervalIncludingBeginEnd(1,100);
//            bool willAddEnergyOverMax = chancePassedFromChanceOnSuccess(attacker->cheerUpSkill->value());
//            if (attacker->haveEnergy(attacker->cheerUpSkill->cost)) {
//                if (willAddEnergyOverMax) {
//                    attacker->curEnergy += energyCountForPlus;
//                    this->roundResultS += attacker->name + " взбодрился, добавив себе " + QString::number(energyCountForPlus) +" энергии ПОВЕРХ ОСНОВНОЙ!";
//                } else {
//                    attacker->curEnergy += energyCountForPlus;
//                    if (attacker->curEnergy > attacker->maxEnergy) {
//                        attacker->curEnergy = attacker->maxEnergy;
//                    }
//                    this->roundResultS += attacker->name + " взбодрился, добавив себе " + QString::number(energyCountForPlus) +" энергии";
//                }
//                attacker->decCurEnergy(attacker->cheerUpSkill->cost);
//            } else {
//                this->roundResultS += attacker->name + " не хватило сил взбодриться! видимо ему приходится нелегко! ";
//            }
//        }
//    }
//    //      TurnSpeed НЕБЛОКИРУЕМЫЙ
//    //      теперь тратим энергию на скорость хода с помю котй достигли таких высот
//    attacker->decCurEnergy(attacker->turnSpeedSkill->cost);

//    //  Заюзаны уже:
//    //  TurnSpeed
//    //  EnergyPlus
//    //      ТЕПЕРЬ ОСТАВШУЮСЯ ЭНЕРГИЮ МОНО ТРАТИТЬ

//    //      НЕЙТРАЛЬНЫЕ
//    bool willDefenderCounterAttack = NO;

//    //      анализ:


//    bool counterAttackStartedAtThisTurn = NO;
//    //      если валяется то попытается встать
//    //bool attackerWasLayingDown = attacker->isLayingDown;//       атакующий валялся в начале своего хода
//    if (attacker->isLayingDown) {
//        int attackerStandUpChance = attacker->standUpChance();
//        bool willAttackerStandUp  = chancePassedFromChanceOnSuccess(attackerStandUpChance);
//        if (willAttackerStandUp) {
//            int standUpEnergyCost = 25;
//            if (attacker->haveEnergy(standUpEnergyCost)) {//подъем ведь за энергию
//                attacker->isLayingDown = NO;//      встал
//                roundResultS += "Атакер сумел встать с шансом " + QString::number(attackerStandUpChance) + " !";
//                attacker->decCurEnergy(standUpEnergyCost);
//            } else {
//                roundResultS += attacker->name + " мог встать, но ему не хватило сил !";
//            }
//        } else {
//            roundResultS += "Атакер НЕ ВСТАЛ с шансом " + QString::number(attackerStandUpChance);
//        }
//    } else {//      атакер стоит и будет бить
//        /*bool possibleToCounterAttack = defender->counterAttackSkill->isLearned() && !attacker->counterAttackInProgress;
//        if (possibleToCounterAttack) {
//            willDefenderCounterAttack = chancePassedFromChanceOnSuccess(defender->counterAttackSkill->chance);
//            if (!defender->haveEnergy(defender->counterAttackSkill->cost)) {
//                this->roundResultS += defender->name + " мог контратаковать но нет сил.";
//            } else {
//                willDefenderCounterAttack = YES;
//                defender->counterAttackInProgress = YES;
//                counterAttackStartedAtThisTurn = YES;
//                this->roundResultS += defender->name + " КОНТРАТАКУЕТ!";
//                //                     attackerResultS += "Я контратакован!\n";
//                //                     defenderResultS += "Контратакую!\n";
//            }
//        }

//        if (!willDefenderCounterAttack) {
//            //Bodypart *attackTargetBodypart = defender->findAliveTargetBodypartForAttack();
//            //int attackTargetBodypartDodgeChance = defender->dodgeChanceOfBodypart(attackTargetBodypart);

//            //      атакующий пытается применить скиллы
//            bool attackerHealSkillsDisabled = NO;
//            bool attackerAttackSkillsDisabled = NO;

//            bool defenderDefenceSkillsDisabled = NO;

//            //      У ОБОИХ ЕСТЬ ШАНС ЗАБЛОЧИТЬ НАПРАВЛЕНИЯ СКИЛОВ ВРАГА. БЛОЧИМ
//            defenderDefenceSkillsDisabled = chancePassedFromChanceOnSuccess(attacker->disableEnemyDefenceSkillsSkill->chance);
//            if (!defender->isLayingDown) {
//                //      тада дефендер попробует отрубить скиллы у атакера
//                attackerHealSkillsDisabled = chancePassedFromChanceOnSuccess(defender->disableEnemyHealSkillsSkill->chance);
//                attackerAttackSkillsDisabled = chancePassedFromChanceOnSuccess(defender->disableEnemyAttackSkillsSkill->chance);
//            }
//            //      щас мы знаем у кого из них вырублены все скилы, а если не все, то какие направления скилов
//            int damage = 100;
//            //      раз зашли знач атакер могет скиловать
//            //      пытаемся применить скилы атакера коте применяются в его ход


//            //      каждым из них пробуем применить незаблоченные скилы
//            //      ХИЛЯЩИЕ
//            if (!attackerHealSkillsDisabled) {
//                bool willAttackerHeal = attacker->healSkill->isLearned();
//                if (attacker->healSkill->isLearned()) {
//                     willAttackerHeal = chancePassedFromChanceOnSuccess(attacker->healSkill->chance);
//                     if (willAttackerHeal) {
//                        pickableAttackerActivatedSkills.append(attacker->healSkill);
//                     }
//                }
//                bool willAttackerHealByStrike = NO;
//                if (attacker->healByStrikeSkill->isLearned()) {
//                     willAttackerHealByStrike = chancePassedFromChanceOnSuccess(attacker->healByStrikeSkill->chance);
//                     if (willAttackerHealByStrike) {
//                         pickableAttackerActivatedSkills.append(attacker->healByStrikeSkill);
//                     }
//                }
//                bool willAttackerHealEnchance = NO;
//                if (attacker->healEnchanceSkill->isLearned()) {
//                     willAttackerHealEnchance = chancePassedFromChanceOnSuccess(attacker->healEnchanceSkill->chance);
//                     if (willAttackerHealEnchance) {
//                        pickableAttackerActivatedSkills.append(attacker->healEnchanceSkill);
//                     }
//                }
//                bool willAttackerHealPercent = NO;
//                if (attacker->healPercentSkill->isLearned()) {
//                     willAttackerHealPercent = chancePassedFromChanceOnSuccess(attacker->healPercentSkill->chance);
//                     if (willAttackerHealPercent) {
//                        pickableAttackerActivatedSkills.append(attacker->healPercentSkill);
//                     }
//                }
//                bool willAttackerMiniHeal = NO;
//                if (attacker->miniHealSkill->isLearned()) {
//                     willAttackerMiniHeal = chancePassedFromChanceOnSuccess(attacker->miniHealSkill->chance);
//                     if (willAttackerMiniHeal) {
//                        pickableAttackerActivatedSkills.append(attacker->miniHealSkill);
//                     }
//                }
//            }

//            //      АТАКУЮЩИЕ
//            if (!attackerAttackSkillsDisabled) {
//                bool willAttackerAccuracyWithContusion= NO;
//                if (attacker->accuracyWithContusionSkill->isLearned()) {
//                     willAttackerAccuracyWithContusion = chancePassedFromChanceOnSuccess(attacker->accuracyWithContusionSkill->chance);
//                     if (willAttackerAccuracyWithContusion) {
//                        pickableAttackerActivatedSkills.append(attacker->accuracyWithContusionSkill);
//                     }
//                }
//                bool willAttackerAttackPower= NO;
//                if (attacker->attackPowerSkill->isLearned()) {
//                     willAttackerAttackPower = chancePassedFromChanceOnSuccess(attacker->attackPowerSkill->chance);
//                     if (willAttackerAttackPower) {
//                        pickableAttackerActivatedSkills.append(attacker->attackPowerSkill);
//                     }
//                }
//                bool willAttackerBash= NO;
//                if (attacker->bashSkill->isLearned()) {
//                     willAttackerBash = chancePassedFromChanceOnSuccess(attacker->bashSkill->chance);
//                     if (willAttackerBash) {
//                        pickableAttackerActivatedSkills.append(attacker->bashSkill);
//                     }
//                }
//                bool willAttackerCritical= NO;
//                if (attacker->criticalSkill->isLearned()) {
//                     willAttackerCritical = chancePassedFromChanceOnSuccess(attacker->criticalSkill->chance);
//                     if (willAttackerCritical) {
//                        pickableAttackerActivatedSkills.append(attacker->criticalSkill);
//                     }
//                }
//                bool willAttackerHitAnyway= NO;
//                if (attacker->hitAnywaySkill->isLearned()) {
//                     willAttackerHitAnyway = chancePassedFromChanceOnSuccess(attacker->hitAnywaySkill->chance);
//                     if (willAttackerHitAnyway) {
//                        pickableAttackerActivatedSkills.append(attacker->hitAnywaySkill);
//                     }
//                }
//                bool willAttackerMultipleAttack= NO;
//                if (attacker->multipleAttackSkill->isLearned()) {
//                     willAttackerMultipleAttack = chancePassedFromChanceOnSuccess(attacker->multipleAttackSkill->chance);
//                     if (willAttackerMultipleAttack) {
//                        pickableAttackerActivatedSkills.append(attacker->multipleAttackSkill);
//                     }
//                }
//                bool willAttackerPierceArmor= NO;
//                if (attacker->pierceArmorSkill->isLearned()) {
//                     willAttackerPierceArmor = chancePassedFromChanceOnSuccess(attacker->pierceArmorSkill->chance);
//                     if (willAttackerPierceArmor) {
//                        pickableAttackerActivatedSkills.append(attacker->pierceArmorSkill);
//                     }
//                }
//            }

//            bool willDefenderDodge = NO;
//            if (defender->dodgeSkill->isLearned()) {
//                 willDefenderDodge = chancePassedFromChanceOnSuccess(defender->dodgeSkill->chance);
//                 if (willDefenderDodge) {
//                    pickableDefenderActivatedSkills.append(attacker->dodgeSkill);
//                 }
//            }
//            if (!defenderDefenceSkillsDisabled) {
//                bool willDefenderConfuseEnemy = NO;
//                if (defender->confuseEnemySkill->isLearned()) {
//                     willDefenderConfuseEnemy = chancePassedFromChanceOnSuccess(defender->confuseEnemySkill->chance);
//                     if (willDefenderConfuseEnemy) {
//                        pickableDefenderActivatedSkills.append(attacker->confuseEnemySkill);
//                     }
//                }

//                bool willDefenderParry= NO;
//                if (defender->parrySkill->isLearned()) {
//                     willDefenderParry = chancePassedFromChanceOnSuccess(defender->parrySkill->chance);
//                     if (willDefenderParry) {
//                        pickableDefenderActivatedSkills.append(attacker->parrySkill);
//                     }
//                }
//                bool willDefenderReduceEnemyDamage= NO;
//                if (defender->reduceEnemyDamageSkill->isLearned()) {
//                     willDefenderReduceEnemyDamage = chancePassedFromChanceOnSuccess(defender->reduceEnemyDamageSkill->chance);
//                     if (willDefenderReduceEnemyDamage) {
//                        pickableDefenderActivatedSkills.append(attacker->reduceEnemyDamageSkill);
//                     }
//                }
//                bool willDefenderReturnDamage= NO;
//                if (defender->returnDamageSkill->isLearned()) {
//                     willDefenderReturnDamage = chancePassedFromChanceOnSuccess(defender->returnDamageSkill->chance);
//                     if (willDefenderReturnDamage) {
//                        pickableDefenderActivatedSkills.append(attacker->returnDamageSkill);
//                     }
//                }
//            }


//            //      ТК АТАКА СВЕРШИЛАСЬ, ТО ДЕЙСТВИЯ:

//            QList <Skill*> appliedAttackerSkills;
//            QList <Skill*> appliedDefenderSkills;
//            //      СПЕРВА ХИЛЯЕТСЯ
//            if (appliedAttackerSkills.contains(attacker->healSkill)) {
//                Bodypart *damagedAliveBodypart = attacker->randomDamagedAliveBodypart();
//                int healHP = attacker->healSkill->value();
//                damagedAliveBodypart->incHP(healHP);
//                roundResultS += attacker->name + " полечил (heal) " + damagedAliveBodypart->name + " на " + QString::number(healHP);
//                attacker->decCurEnergy(attacker->healSkill->cost);
//            }
//            if (appliedAttackerSkills.contains(attacker->healEnchanceSkill)) {
//                Bodypart *aliveBodypart = attacker->randomAliveBodypart();
//                int healHP = attacker->healEnchanceSkill->value();
//                aliveBodypart->setMaxHP(aliveBodypart->maxHP() + healHP);
//                aliveBodypart->incHP(healHP);
//                roundResultS += attacker->name + " прибавил (healEnchance) " + QString::number(healHP) + "к макс и тек хп " + aliveBodypart->name;
//                attacker->decCurEnergy(attacker->healEnchanceSkill->cost);
//            }
//            if (appliedAttackerSkills.contains(attacker->healPercentSkill)) {
//                Bodypart *bodypart = attacker->randomBodypart();
//                int healPercentHP = attacker->healPercentSkill->value();
//                int healHP = round((float)bodypart->maxHP() * healPercentHP / 100.0);
//                if (bodypart->isAlive()) {
//                    bodypart->incHP(healHP);
//                    roundResultS += attacker->name + " полечил (healPercent) " + bodypart->name + " на " + QString::number(healPercentHP) + " % ( " + QString::number(healHP) + " ) ";
//                }
//                attacker->decCurEnergy(attacker->healPercentSkill->cost);
//            }
//            if (appliedAttackerSkills.contains(attacker->miniHealSkill)) {
//                Bodypart *aliveBodypart = attacker->randomAliveBodypart();
//                int healHP = attacker->miniHealSkill->value();
//                aliveBodypart->incHP(healHP);
//                roundResultS += attacker->name + " полечил (miniHeal)" + aliveBodypart->name + " на " + QString::number(healHP);
//                attacker->decCurEnergy(attacker->miniHealSkill->cost);
//            }
//            if (appliedAttackerSkills.contains(attacker->healRepairSkill)) {
//                Bodypart *bodypart = attacker->randomBodypart();
//                int healPercentHP = attacker->healPercentSkill->value();
//                int healHP = round((float)bodypart->maxHP() * healPercentHP / 100.0);
//                if (!bodypart->isAlive()) {
//                    bodypart->repairIncHP(healHP);
//                    roundResultS += attacker->name + " починил (healRepair) " + bodypart->name + " на " + QString::number(healPercentHP) + " % ( " + QString::number(healHP) + " ) ";
//                }
//                attacker->decCurEnergy(attacker->healRepairSkill->cost);
//            }
//            bool isDefenderDodged = NO;
//            if (!appliedAttackerSkills.contains(attacker->hitAnywaySkill)) {
//                if (appliedDefenderSkills.contains(defender->confuseEnemySkill)) {
//                    isDefenderDodged = YES;
//                    this->roundResultS += defender->name + " УВЕРНУЛСЯ с шансом " + QString::number(defender->dodgeSkill->chance);
//                }
//            }
//            if (!isDefenderDodged) {

//            }
//        }*/
//    }
//    //      ЗАЩИТНЫЕ



/*
    //      знач он м токо просто ударить тада выбираем часть тела,
    //      считаем верть уворота этой ЧТ и смотрим попал или нет
   if (attackTargetBodypartDodgeChance < 100) {
       int chanceToHit = 100 - attackTargetBodypartDodgeChance;
       bool willAttackerDealDamage = chancePassedFromChanceOnSuccess(chanceToHit);
       bool willThrowDefenderDown = NO;
       if (willAttackerDealDamage) {
           attackTargetBodypart->forceDamage(damage);
           roundResultS += "Атакующий нанес " + QString::number(damage) + " урона по " + attackTargetBodypart->name + " обороняющегося.\n";
           //      тк атакер не валялся то своими действиями попытается свалить дефендера
           int throwDefenderDownChance = 100 - attacker->standUpChance();// верть свалить врага
           if (throwDefenderDownChance < 100) {
               //      атакующему требся для этого на 10 энергии больше
               willThrowDefenderDown = chancePassedFromChanceOnSuccess(throwDefenderDownChance);
               if (willThrowDefenderDown) {
                   defender->isLayingDown = YES;
                   roundResultS += "Атакер свалил дефендера на землю c шансом " + QString::number(throwDefenderDownChance);
               }
           }
       }
       if (!willAttackerDealDamage) {
           roundResultS += attackTargetBodypart->name + " обороняющегося увернулась с вероятностью " + attackTargetBodypartDodgeChance + " !\n";
       }

    */



    //      конец хода. понижаем длительность эффектов и снимаем истекшие
//    if (!counterAttackStartedAtThisTurn) {
//        attacker->counterAttackInProgress = NO;//       если контратака запустилась то все равно щас контратакующий дефендер. а када он станет атакующим то после его атаки станет 0 контратакующих в процессе. тк если кл был контратакующим то щас он атакующий //       цель невозмти сити бескй контратаки када атакующий атаковал, защитник контратаковал. ход снова переходит
//    }
    //attacker->setSkillsDisabledRemainedTurns(attacker->skillsDisabledRemainedTurns() - 1);
    //defender->setSkillsDisabledRemainedTurns(defender->skillsDisabledRemainedTurns() - 1);

    //      к атакующему а защитник снова контратакует. контратакер способен контратаковать токо если он уже выполнил контратаку и после этого его соп сделал полный ход. также контратака не м сробить када
    //      один из персов уже контратакер. а то получся так: атакующий бьет, защитник контратакует и тут атакующий контратакует контратакера хаха

}



void Fight::finishBattle() {
    Fighter *winner = fighter1;
    int winnerTurnCount = this->fighter1FinishedTurnCount;
    int looserTurnCount = this->fighter2FinishedTurnCount;

    if (fighter1->head->curHP() <= 0) {
        winner = fighter2;
        winnerTurnCount = this->fighter2FinishedTurnCount;
        looserTurnCount = this->fighter1FinishedTurnCount;
    }
    this->roundResultS += "Выиграл персонаж " + winner->name + "за " + QString::number(winnerTurnCount) +" . А проигравший сходил " + QString::number(looserTurnCount) + "раз";
}



